#include "trafo.h"
#include "types.h"


extern unsigned char rgb2gray(pixelRGB* pixel);

void RGB2GrayMatrix(pixelRGB orig[], unsigned char dest[], int nfilas, int ncols) {
	int i,j;
    for (i = 0;i < nfilas; i++)
    	for(j=0;j < ncols;j++) {
    		pixelRGB pixel = orig[i * ncols + j];
    		dest[i * ncols + j] = rgb2gray(&pixel);
    	}
}

void Gray2BinaryMatrix(unsigned char orig[], unsigned char dest[], unsigned char umbral, int nfilas, int ncols) {
	int i,j;
		for (i=0;i<nfilas;i++) {
			for (j=0;j<ncols;j++) {
					if (orig [i*ncols + j] > umbral)
						dest [i*ncols + j]= 0xFF;
					else
						dest[i*ncols + j]= 0x00;
			}
		}
}

extern void computeHistogram(unsigned char imagenGris[],short int histogram[],int N,int M) {
	int i,j;
	for (i=0;i<256;i++) {
		histogram[i]=0;
	}
	for (i=0;i<N;i++) {
		for (j=0;j<M;j++) {
			histogram[ imagenGris[i*M + j] ] ++ ;
		}
	}
}

unsigned char computeThreshold(short int histogram[]) {
	short int max=-1, max2=-1;
	int max_idx=-1, max2_idx=-1;
	int i;
	for (i=0;i<128;i++) {
		if (histogram[i] > max) {
			max = histogram[i];
			max_idx = i;
		}
	}
	for (i=128;i<255;i++) {
		if (histogram[i] > max2) {
			max2 = histogram[i];
			max2_idx = i;
		}
	}
	return max_idx + (max2_idx - max_idx)/2;
}

void contarUnos(unsigned char mat[], short int vector[],int nfilas, int ncols) {
	int i,j;
	for (i=0;i<nfilas;i++) {
		for (j=0;j<ncols;j++) {
				if (mat [i*ncols + j] == 0xFF)
					vector [i]++;
		}
	}
}

