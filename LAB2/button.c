/*-------------------------------------------------------------------
**
**  Fichero:
**    button.c  10/6/2014
**
**    Estructura de Computadores
**    Dpto. de Arquitectura de Computadores y Autom�tica
**    Facultad de Inform�tica. Universidad Complutense de Madrid
**
**  Prop�sito:
**    Contiene las implementaciones de las funciones
**    para la gesti�n de los pulsadores de la placa de prototipado
**
**  Notas de dise�o:
**
**-----------------------------------------------------------------*/

#include "44b.h"
#include "utils.h"
#include "button.h"
#include "leds.h"


// Configuraci�n de la botonera
// - Habilitar EINT4567 por FIQ
// - Configura puerto G para generar interrupciones por EINT4567 por flanco de bajada (PCONG - EXTINT)
// - Configurar resistencia de pull-up
// - Desenmascarar el bit EINT4567 para permitr interrupciones por esa linea
void button_init( void )
{
	/*Activates FIQ line */
	//rINTCON = 0x1;
	rINTCON = 0x0;

	/*Activates FIQ mode in all the lines */
	rINTMOD = 0xffffffff;

	/*Configure pines 6 and 7  of port G as EINT6 and EINT7 */
	rPCONG = 0xffff;

	/*Generate interruptions in falling edge in EINT4567 */
	//rEXTINT = 0x24000000;
	rEXTINT = 0x22000000;

	/*Explanation of rEXTINT
	 * [30]	  				   [27]    [26]
 	    010						0		010	 0000 0000 0000 0000 0000 0000
        Falling edge in EINT7			Falling edge in EINT6 */

	/*Activates pull-up resistance*/
	 rPUPG=0x0;

	 /*Unmask EINT4567 in rINTMSK (Disable interruptions in this line) */
	 rINTMSK = BIT_EINT4567;

}


// COMPLETAR LA FUNCION
// Almacenar en which_int el origen de la interrupcion:
//  - 4 para un boton, 8 para otro
// Al finalizar, borrar la interrupci�n en EXTINTPND
void DoDetecta(void) 
{
	int which_int;

	/* Idenficiar la interrupcion */
	which_int = rEXTINTPND;;

	/* Espera fin de rebote de presi�n */
	// Delay hace espera activa. Tiempo expresado en unidades de 100us (2000 -> 200 ms)
    Delay( 2000 );

	/* Encender/apagar el LED adecuado */

    switch(which_int){
    case 4:
    	switchLed1();
		break;
    case 8:
    	switchLed2();
		break;
    default:
    	break;
    }

   /* Finalizar ISR */

    rEXTINTPND = which_int;
    /*
    rEXTINTPND = 0xf;			//Borra los bits en EXTINTPND
    rF_ISPC |= BIT_EINT4567;	//Borra el bit pendiente en INTPND;
    */



}

