/*-----------------------------------------------------------------
**
**  Fichero:
**    init.asm  10/6/2014
**
**    Estructura de Computadores
**    Dpto. de Arquitectura de Computadores y Autom�tica
**    Facultad de Inform�tica. Universidad Complutense de Madrid
**
**  Prop�sito:
**    Arranca un programa en C
**
**  Notas de dise�o:
**
**---------------------------------------------------------------*/

    .extern main
    .extern ISR_Undef
	.extern ISR_SWI
	.extern ISR_Pabort
	.extern ISR_Dabort
	.extern ISR_IRQ
	.extern DoDetecta

	.global start
	
	.global DoUndef
    .global DoSWI
    .global DoDabort

/* Tabla en la que escribir las direcciones de las ISRs */
.equ 	_ISR_STARTADDRESS,	0xc7fff00		/* GCS6:64M DRAM/SDRAM 	*/
.equ    HandleReset,    _ISR_STARTADDRESS
.equ    HandleUndef,    _ISR_STARTADDRESS+4
.equ    HandleSWI,      _ISR_STARTADDRESS+4*2
.equ    HandlePabort,   _ISR_STARTADDRESS+4*3
.equ    HandleDabort,   _ISR_STARTADDRESS+4*4
.equ    HandleReserved, _ISR_STARTADDRESS+4*5
.equ    HandleIRQ,      _ISR_STARTADDRESS+4*6
.equ    HandleFIQ,      _ISR_STARTADDRESS+4*7

/* Constantes utiles para la rutina ISR_FIQ */
.equ INTPND, 0x1e00004
.equ EXTINTPND, 0x1d20054
.equ F_ISPC, 0x1e0003c

	/*
	** Modos de operaci�n
	*/
	.equ MODEMASK, 0x1f		/* Para selecci�n de M[4:0] del CPSR */
	.equ USRMODE,  0x10
	.equ FIQMODE,  0x11
	.equ IRQMODE,  0x12
	.equ SVCMODE,  0x13
	.equ ABTMODE,  0x17
	.equ UNDMODE,  0x1b
	.equ SYSMODE,  0x1f

	/*
	** Direcciones de las bases de las pilas del sistema 
	*/
	.equ USRSTACK, 0xc7ff000   	
	.equ SVCSTACK, 0xc7ff100
	.equ UNDSTACK, 0xc7ff200
	.equ ABTSTACK, 0xc7ff300
	.equ IRQSTACK, 0xc7ff400
	.equ FIQSTACK, 0xc7ff500

	/*
	** Registro de m�scara de interrupci�n
	*/
	.equ rINTMSK,    0x1e0000c

start:

	/* Pasa a modo supervisor */
    mrs	r0, cpsr
    bic	r0, r0, #MODEMASK
    orr	r1, r0, #SVCMODE
    msr	cpsr_c, r1 

	/* Enmascara interrupciones */
	ldr r0, =rINTMSK
	ldr r1, =0x1fffffff
    str r1, [r0]

	/* Habilita linea IRQ y FIQ del CPSR */
	mrs r0, cpsr
	bic r0, r0, #0xC0
	msr cpsr_c, r0
	
	/* Desde modo SVC inicializa los SP de todos los modos de ejecuci�n privilegiados */
    bl InitStacks
	
	/* Escribe en la tabla de ISR las direcciones de las rutinas de tratamiento de excepcion*/
	bl InitISR

	/* Desde modo SVC cambia a modo USR e inicializa el SP_usr */
	mrs r0, cpsr
	bic r0, r0, #MODEMASK
	orr r1, r0, #USRMODE  
	msr cpsr_c, r1
	ldr sp, =USRSTACK

    //mov fp, #0

    bl main

End:
    B End


InitStacks:

	/* Pasa a modo Undef e inicializa el SP_und */

    mrs	r0, cpsr				/* Llevamos el registro de estado a r0 */
    bic	r0, r0, #MODEMASK		/* Borramos los bits de modo de r0 */
    orr	r1, r0, #UNDMODE		/* A�adimos el c�digo del modo Undef y copiamos en r1 */
    msr	cpsr_c, r1				/* Escribimos el resultado en el registro de estado */
    ldr sp, =UNDSTACK			/* Copiamos la	direcci�n de comienzo de la pila */

	/* Pasa a modo Abort e inicializa el SP_abt */

    mrs	r0, cpsr				/* Llevamos el registro de estado a r0 */
    bic	r0, r0, #MODEMASK		/* Borramos los bits de modo de r0 */
    orr	r1, r0, #ABTMODE		/* A�adimos el c�digo del modo Abort y copiamos en r1 */
    msr	cpsr_c, r1				/* Escribimos el resultado en el registro de estado */
    ldr sp, =ABTSTACK			/* Copiamos la	direcci�n de comienzo de la pila */

    /* Pasa a modo IRQ e inicializa el SP_irq */

    mrs	r0, cpsr				/* Llevamos el registro de estado a r0 */
    bic	r0, r0, #MODEMASK		/* Borramos los bits de modo de r0 */
    orr	r1, r0, #IRQMODE		/* A�adimos el c�digo del modo Undef y copiamos en r1 */
    msr	cpsr_c, r1				/* Escribimos el resultado en el registro de estado */
    ldr sp, =IRQSTACK			/* Copiamos la	direcci�n de comienzo de la pila */

	/* Pasa a modo FIQ e inicializa el SP_fiq */

    mrs	r0, cpsr				/* Llevamos el registro de estado a r0 */
    bic	r0, r0, #MODEMASK		/* Borramos los bits de modo de r0 */
    orr	r1, r0, #FIQMODE		/* A�adimos el c�digo del modo Undef y copiamos en r1 */
    msr	cpsr_c, r1				/* Escribimos el resultado en el registro de estado */
    ldr sp, =FIQSTACK			/* Copiamos la	direcci�n de comienzo de la pila */

    /* Pasa a modo SVC e inicializa el SP_svc */

    mrs	r0, cpsr				/* Llevamos el registro de estado a r0 */
    bic	r0, r0, #MODEMASK		/* Borramos los bits de modo de r0 */
    orr	r1, r0, #SVCMODE		/* A�adimos el c�digo del modo Undef y copiamos en r1 */
    msr	cpsr_c, r1				/* Escribimos el resultado en el registro de estado */
    ldr sp, =SVCSTACK			/* Copiamos la	direcci�n de comienzo de la pila */

    mov pc, lr

/** COMPLETAR **/
InitISR:

	/*Inicializar ISR del modo Undef */
	ldr r0, =ISR_Undef
	ldr r1, =HandleUndef
	str r0, [r1]

	/*Inicializar ISR del modo Dabort */
	ldr r0, =ISR_Dabort
	ldr r1, =HandleDabort
	str r0, [r1]

	/*Inicializar ISR del modo Pabort */
	ldr r0, =ISR_Pabort
	ldr r1, =HandlePabort
	str r0, [r1]

	/*Inicializar ISR del modo IRQ */
	ldr r0, =ISR_IRQ
	ldr r1, =HandleIRQ
	str r0, [r1]

	/*Inicializar ISR del modo SWI */
	ldr r0, =ISR_SWI
	ldr r1, =HandleSWI
	str r0, [r1]

	/*Inicializar ISR del modo FIQ */
	ldr r0, =ISR_FIQ
	ldr r1, =HandleFIQ
	str r0, [r1]

	mov pc,lr


/** COMPLETAR
** 1. comprobar si hay interrupcion pendiente por EINT4567 consultando INTPND (por ser FIQ)
** 2. Comprobar si ha sido uno de los dos botones, consultando EXTINPD
** 3. Si es asi, saltar a DoDetecta
** 4. Antes de finalizar, limpiar interrupciones pendientes escribiendo en F_ISPC
** 5. Hacer el retorno de acuerdo a la tabla presentada en el guion
**/
ISR_FIQ:

		//Prolog (Save all registers)
		push {r0-r10,fp,lr}

		//Read INTPND to know if there are pending interruptions
		ldr r0, =INTPND
		ldr r1, [r0]
		mov r0, #1
		lsl r0,r0,#21
		//mov r0, #0x0200000  	/*00 0010 0000 0000 0000 0000 0000  Bit 21 = 1 */
		and r2, r0, r1			/* R2 = 0x0200000 if Bit 21 = 1 */
		cmp r0,r2				//Compare if any button has been pressed
		bne return_ISR_FIQ


		/*Detect from EXTINTPND if we have 1000 (EXTINT7 Pressed)
		or 0100  (EXTINT7 Pressed) */

		ldr r0, =EXTINTPND
		ldr r1, [r0]
		mov r2, #4
		//and r3, r1, r2		Masking to know if EXINT6 has been pressed
		cmp r1,r2				/*Compare if EXINT6 has been activated */
		beq buttonPressed
		mov r2,#8
		//and r3, r1, r2		Masking to know if EXINT7 has been pressed
		cmp r1,r2				/*Compare if EXINT7 has been activated */
		beq buttonPressed
		b return_ISR_FIQ
		buttonPressed:
		bl DoDetecta

		return_ISR_FIQ:

		//Unmask F_ISPC
		ldr r0, =F_ISPC
		ldr r0, [r0]
		mov r1, #1
		lsl r1,r1,#21
		//mov r1, #0x0200000
		orr r1, r0, r1
		ldr r0, =F_ISPC
		str r1, [r0]

		//Epilog (Load all registers)
		pop {r0-r10, fp,lr}
		SUBS PC, lr, #4			//FIQ Return

DoSWI:
	swi #0
	mov pc,lr

DoUndef:
	.word 0xE6000010
	mov pc,lr

DoDabort:
	ldr r0,=0x0a333333
	str r0,[r0]
	mov pc,lr


	.end

